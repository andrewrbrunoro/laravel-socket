## Como utilizar o Socket  
**Cliente side**  
  
**Via REDIS**  

    - redis-cli -h {IP do servidor que está com este repositório}    
     
    - PUBLISH {canal/sala/channel} '{"event": "obrigatório", "data": { ... dados }}'   

  
**Echo**  

    - Echo.join({canal/sala/channel}).listen('.{evento o ponto atrás é obrigatório no ECHO}', (r) => { console.log(r) })

  
**Laravel Broadcast**  
<p>
Quando criar sua classe de evento adicione a interface ShouldBroadcastNow  
    
    - class MessageEvent implements ShouldBroadcastNow {  }
</p>