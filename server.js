var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis({
    password: ''
});

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

redis.psubscribe('*');
redis.on('pmessage',function(subscribed, channel, message) {
    console.log('Channel is ' + channel + ' and message is ' + message);
    if (isJson(message)) {
        message = JSON.parse(message);
        io.emit(channel, message);
    } else {
        return io.emit(channel, message);
    }
});

http.listen(3000, function(){
    console.log('Listening on Port 3000');
});
